
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:snipeitapp/models/asset.dart';
import 'package:snipeitapp/models/category.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final amountTextController = TextEditingController();
  final accountTextController = TextEditingController();


  @override
  Widget build(BuildContext context) {

    return new Scaffold(

      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Container(

          height: MediaQuery.of(context).size.height*0.8,
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              // Center(
              //   child: Padding(
              //     padding: const EdgeInsets.all(8.0),
              //     child: Text("Dashboard", style: GoogleFonts.concertOne(
              //         fontWeight: FontWeight.w500,
              //         // color: Colors.red[900],
              //         fontSize: 20
              //     ),),
              //   ),
              // ),
              SizedBox(height: 20),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    child: Container(
                      height: MediaQuery.of(context).size.width*0.3,
                      width: MediaQuery.of(context).size.width*0.4,
                      margin: EdgeInsets.only(right: 10, left: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.cyan[300],
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 6,
                                spreadRadius: 2,
                                offset: Offset(0.0, 7.0)
                            )
                          ]
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 16,
                              left: 10,
                              child: Text(
                                "46",
                                style: GoogleFonts.concertOne(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w700,
                                  color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                              bottom: 35,
                              left: 10,
                              child: Text(
                                "Total Assets",
                                style: GoogleFonts.concertOne(
                                    fontSize: 15,
                                    // fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                            right: 10,
                            top: 5,
                            child: Icon(Icons.analytics_outlined, size: 75,color: Colors.black12,),

                          ) ,
                          Positioned(
                            bottom: 0,
                            right: 0,
                            left: 0,
                            child: Container(
                              height: 22,
                              color: Colors.black12,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("More Info", style: TextStyle(color: Colors.white60),),
                                  Transform.rotate(
                                      angle: 4.7,
                                      child: Icon(Icons.arrow_drop_down_circle_rounded, size: 16,color: Colors.white60,))
                                ],
                              ),
                            )

                          ) ,
                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    child: Container(
                      height: MediaQuery.of(context).size.width*0.3,
                      width: MediaQuery.of(context).size.width*0.4,
                      margin: EdgeInsets.only(right: 10, left: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.pink[600],
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 6,
                                spreadRadius: 2,
                                offset: Offset(0.0, 7.0)
                            )
                          ]
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 16,
                              left: 10,
                              child: Text(
                                "50",
                                style: GoogleFonts.concertOne(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w700,
                                  color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                              bottom: 35,
                              left: 10,
                              child: Text(
                                "Total License",
                                style: GoogleFonts.concertOne(
                                    fontSize: 15,
                                    // fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                            right: 10,
                            top: 5,
                            child: Icon(Icons.save, size: 75,color: Colors.black12,),

                          ) ,
                          Positioned(
                            bottom: 0,
                            right: 0,
                            left: 0,
                            child: Container(
                              height: 22,
                              color: Colors.black12,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("More Info", style: TextStyle(color: Colors.white60),),
                                  Transform.rotate(
                                      angle: 4.7,
                                      child: Icon(Icons.arrow_drop_down_circle_rounded, size: 16,color: Colors.white60,))
                                ],
                              ),
                            )

                          ) ,
                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),


                ],
              ),

              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    child: Container(
                      height: MediaQuery.of(context).size.width*0.3,
                      width: MediaQuery.of(context).size.width*0.4,
                      margin: EdgeInsets.only(right: 10, left: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.orange[600],
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 6,
                                spreadRadius: 2,
                                offset: Offset(0.0, 7.0)
                            )
                          ]
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 16,
                              left: 10,
                              child: Text(
                                "4",
                                style: GoogleFonts.concertOne(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                              bottom: 30,
                              left: 10,
                              child: Text(
                                "Total Accessories",
                                style: GoogleFonts.concertOne(
                                    fontSize: 15,
                                    // fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                            right: 10,
                            top: 5,
                            child: Icon(Icons.keyboard, size: 75,color: Colors.black12,),

                          ) ,
                          Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: Container(
                                height: 22,
                                color: Colors.black12,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("More Info", style: TextStyle(color: Colors.white60),),
                                    Transform.rotate(
                                        angle: 4.7,
                                        child: Icon(Icons.arrow_drop_down_circle_rounded, size: 16,color: Colors.white60,))
                                  ],
                                ),
                              )

                          ) ,
                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),

                  Align(
                    child: Container(
                      height: MediaQuery.of(context).size.width*0.3,
                      width: MediaQuery.of(context).size.width*0.4,
                      margin: EdgeInsets.only(right: 10, left: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.deepPurple,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 6,
                                spreadRadius: 2,
                                offset: Offset(0.0, 7.0)
                            )
                          ]
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 16,
                              left: 10,
                              child: Text(
                                "3",
                                style: GoogleFonts.concertOne(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                              bottom: 30,
                              left: 10,
                              child: Text(
                                "Total Consumables",
                                style: GoogleFonts.concertOne(
                                    fontSize: 15,
                                    // fontWeight: FontWeight.w700,
                                    color: Colors.white
                                ),

                              )
                          ) ,
                          Positioned(
                            right: 10,
                            top: 5,
                            child:   Transform.rotate(
                                angle: 3.14,
                                child: Icon(Icons.location_on, size: 75, color: Colors.black12,)),

                          ) ,
                          Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: Container(
                                height: 22,
                                color: Colors.black12,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("More Info", style: TextStyle(color: Colors.white60),),
                                    Transform.rotate(
                                        angle: 4.7,
                                        child: Icon(Icons.arrow_drop_down_circle_rounded, size: 16,color: Colors.white60,))
                                  ],
                                ),
                              )

                          ) ,
                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),


                ],
              ),
              SizedBox(height: 10,),
              // Divider(),
              //
              // Padding(
              //   padding: const EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 10),
              // child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Padding(
              //         padding: const EdgeInsets.only(left: 30, bottom: 10),
              //         child: Text(
              //           "Recent Activities",
              //           style: GoogleFonts.concertOne(
              //             fontSize: 16,
              //             fontWeight: FontWeight.w400,
              //           ),),
              //       ),
              //       Container(
              //         height: MediaQuery.of(context).size.height*0.4,
              //         child: ListView.builder(
              //           // padding: EdgeInsets.only(right: 16, left: 16),
              //             scrollDirection: Axis.vertical,
              //             itemCount: assets.length,
              //             itemBuilder: (context, index){
              //               return Padding(
              //                 padding: const EdgeInsets.all(8.0),
              //                 child: Container(
              //                   margin: EdgeInsets.only(right: 20, left: 20),
              //                   height: 80,
              //                   width: 120,
              //                   decoration: BoxDecoration(
              //                     borderRadius: BorderRadius.circular(10),
              //                     // color: Colors.blueGrey[50],
              //                     color: Colors.blue[100],
              //
              //
              //
              //                   ),
              //                   child: Stack(
              //                     children: [
              //
              //                       Positioned(
              //                           top: 15,
              //                           left: 10,
              //                           child: Text(
              //                             assets[index].target,
              //                             style: GoogleFonts.concertOne(
              //                                 fontSize: 14,
              //                                 fontWeight: FontWeight.w700,
              //                                 // color: kBlackColor
              //                             ),)
              //                       ),
              //                       Positioned(
              //                           bottom: 10,
              //                           left: 10,
              //                           child: Text(
              //                             assets[index].admin,
              //                             style: GoogleFonts.concertOne(
              //                                 fontSize: 16,
              //                                 fontWeight: FontWeight.w500,
              //                                 color: Colors.blueGrey
              //
              //                               // color: kBlackColor
              //                             ),)
              //                       ),
              //                       Positioned(
              //                           top: 15,
              //                           right: 10,
              //                           child: Text(
              //                             assets[index].date,
              //                             style: GoogleFonts.concertOne(
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.w300,
              //                             ),)
              //                       ),
              //                       Positioned(
              //                           bottom: 10,
              //                           right: 10,
              //                           child: Text(
              //                             assets[index].item,
              //                             style: GoogleFonts.concertOne(
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.w500,
              //                             ),)
              //                       ),
              //
              //
              //                     ],
              //                   ),
              //                 ),
              //               );
              //             }),
              //       ),
              //     ],
              //   ),
              // ),
              // Divider(),
              // Padding(
              //   padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
              // child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Padding(
              //         padding: const EdgeInsets.only(left: 30, bottom: 10),
              //         child: Text(
              //           "Asset Categories",
              //           style: GoogleFonts.concertOne(
              //             fontSize: 16,
              //             fontWeight: FontWeight.w400,
              //           ),),
              //       ),
              //       Container(
              //         height: MediaQuery.of(context).size.height*0.4,
              //         child: ListView.builder(
              //           // padding: EdgeInsets.only(right: 16, left: 16),
              //             scrollDirection: Axis.vertical,
              //             itemCount: category.length,
              //             itemBuilder: (context, index){
              //               return Padding(
              //                 padding: const EdgeInsets.all(8.0),
              //                 child: Container(
              //                   margin: EdgeInsets.only(right: 20, left: 20),
              //                   height: 80,
              //                   width: 120,
              //                   decoration: BoxDecoration(
              //                     borderRadius: BorderRadius.circular(10),
              //                     // color: Colors.blueGrey[50],
              //                     color: Colors.deepPurple[100],
              //
              //
              //
              //                   ),
              //                   child: Stack(
              //                     children: [
              //
              //                       Positioned(
              //                           top: 15,
              //                           left: 10,
              //                           child: Text(
              //                             category[index].name,
              //                             style: GoogleFonts.concertOne(
              //                                 fontSize: 18,
              //                                 fontWeight: FontWeight.w700,
              //                                 color: Colors.white
              //                             ),)
              //                       ),
              //                       Positioned(
              //                           bottom: 10,
              //                           left: 10,
              //                           child: Text(
              //                             category[index].type,
              //                             style: GoogleFonts.concertOne(
              //                                 fontSize: 16,
              //                                 fontWeight: FontWeight.w500,
              //                                 color: Colors.blueGrey
              //
              //                               // color: kBlackColor
              //                             ),)
              //                       ),
              //                       Positioned(
              //                           top: 15,
              //                           right: 30,
              //                           child: Text(
              //                             category[index].quantity,
              //                             style: GoogleFonts.concertOne(
              //                               fontSize: 16,
              //                               fontWeight: FontWeight.w700,
              //                             ),)
              //                       ),
              //                       // Positioned(
              //                       //     bottom: 10,
              //                       //     right: 10,
              //                       //     child: Icon(
              //                       //       Icons."category[index].quantity"
              //                       //     )
              //                       // ),
              //
              //
              //                     ],
              //                   ),
              //                 ),
              //               );
              //             }),
              //       ),
              //     ],
              //   ),
              // ),




            ],
          ),
        ),
      ),

    );

  }



}

