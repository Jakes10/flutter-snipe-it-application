
import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

class Assets extends StatefulWidget {
  @override
  _AssetsState createState() => _AssetsState();
}

class _AssetsState extends State<Assets> {
  String dropDownValue;
  List<String> cityList = [
    'Edit',
    'Delete',
    'Generate Labels',
  ];

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Container(

          height: MediaQuery.of(context).size.height*0.8,
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 30, ),
                    child: Container(
                      // color: kWhiteColor,

                      width:180,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,

                        children: [
                          DropdownButtonFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(10.0),
                                ),
                              ),
                              contentPadding: EdgeInsets.fromLTRB(15, 5.5, 5, 0),
                              // fillColor: Colors.white,
                              filled: true,
                              hintStyle: new TextStyle(color: Colors.black38, fontSize: 14, fontWeight: FontWeight.w500),
                              hintText: "Select Action",

                              // fillColor: Colors.blue[200]
                            ),

                            value: dropDownValue,
                            onChanged: (String Value) {
                              setState(() {
                                dropDownValue = Value;
                              });
                            },
                            items: cityList
                                .map((cityTitle) => DropdownMenuItem(
                                value: cityTitle, child: Text("$cityTitle")))
                                .toList(),
                          ),

                          SizedBox(height: 15,),
                        ],
                      ),
                    ),
                  ),
//                   Container(
//                     width: 50,
//                     child: MaterialButton(
//
//                       color: Colors.blueAccent,
//                       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
// //      borderSide: BorderSide(color: selectedIndex == index ? Colors.cyan : Colors.grey),
//                       child: Text("Go",style: TextStyle(color: Colors.white, fontSize: 16),),
//
//                     ),
//                   )
                ],
              ),



              SizedBox(height: 20),



            ],
          ),
        ),
      ),

    );

  }



}

