
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:google_fonts/google_fonts.dart';

class Activity extends StatefulWidget {
  @override
  _ActivityState createState() => _ActivityState();
}

class _ActivityState extends State<Activity> {
  final amountTextController = TextEditingController();
  final accountTextController = TextEditingController();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title:  Text("Menu", style: TextStyle(color: Colors.red),   textAlign: TextAlign.center,),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
            color: Colors.red
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Container(

          height: MediaQuery.of(context).size.height*0.8,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 20),
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: [
                // Container(
                //
                //   child: Row(
                //     children: [
                //      GestureDetector(
                //        onTap: ()=>{
                //        Navigator.of(context).pop()
                //
                //      },
                //          child: Icon(Icons.arrow_back)
                //      ),
                //     ],
                //   ),
                // ),
                Container(
                  // color: kWhiteColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: [
                      // Padding(
                      //     padding: EdgeInsets.only(left: 24, top: 8, bottom: 16),
                      //     child: Text("Activity")
                      // ),
                      Row(
                        children: [
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.playlist_add_check, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "CHECK-IN",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.person, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "CHECK-OUT",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.location_on, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "LOCATION",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),


                        ],
                      ),

                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.settings, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "MAINTENANCE",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.access_time, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "AUDIT",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.warning, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "DUE TO AUDIT",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),


                        ],
                      ),
                      SizedBox(height: 15,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Align(
                            child: Container(
                              height: MediaQuery.of(context).size.width*0.25,
                              width: MediaQuery.of(context).size.width*0.275,
                              margin: EdgeInsets.only(right: 10, left: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  color: Colors.black,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 6,
                                        spreadRadius: 2,
                                        offset: Offset(0.0, 7.0)
                                    )
                                  ]
                              ),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(Icons.assignment_outlined, color: Colors.white,size: 40,),
                                        SizedBox(height: 10,),
                                        Text(
                                          "LOAD LIST",
                                          style: GoogleFonts.concertOne(
                                              fontSize: 15,
                                              // fontWeight: FontWeight.w700,
                                              color: Colors.white
                                          ),

                                        )
                                      ],
                                    ),
                                  ) ,

                                  SizedBox(height: 15,),
                                ],
                              ),
                            ),
                          ),


                        ],
                      ),

                    ],
                  ),
                ),
                SizedBox(height: 20),



              ],
            ),
          ),
        ),
      ),

    );

  }



}

