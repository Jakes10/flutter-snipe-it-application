import 'package:flutter/material.dart';
import 'package:snipeitapp/views/accessories.dart';
import 'package:snipeitapp/views/actions.dart';
import 'package:snipeitapp/views/assets.dart';
import 'package:snipeitapp/views/consumables.dart';
import 'package:snipeitapp/views/dashboard.dart';
import 'package:snipeitapp/views/licenses.dart';
import 'package:snipeitapp/views/actions.dart';
import 'package:google_fonts/google_fonts.dart';



class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex=0;
  final tabs =[
    new Dashboard(),
    new Assets(),
    new License(),
    new Accessories(),
    new Consumables()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        unselectedFontSize: 12,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 15,

        selectedItemColor: Theme.of(context).accentColor,
        iconSize: 24,
        currentIndex: _currentIndex,
        items:[
          BottomNavigationBarItem(
            icon: new Icon(Icons.dashboard),
            title: new Text('Dashboard',
              style: GoogleFonts.concertOne(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.analytics_rounded),
            title: new Text('Assets',
              style: GoogleFonts.concertOne(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.save),
            title: new Text('Licenses',
              style: GoogleFonts.concertOne(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.keyboard),
              title: Text('Accessories',
                style: GoogleFonts.concertOne(
                  fontWeight: FontWeight.w500,
                ),
              )
          ),
          // BottomNavigationBarItem(
          //     icon: Icon(Icons.apps_rounded),
          //     title: Text('Consumables',
          //       style: GoogleFonts.concertOne(
          //         fontWeight: FontWeight.w500,
          //       ),
          //     )
          // )
        ],
        onTap: (index){
          setState(() {
            _currentIndex=index;
          });
        },
      ),
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: IconThemeData(
            color: Theme.of(context).accentColor
        ),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[



            new ListTile(
              // leading: Icon(Icons.dashboard),
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.dashboard, color: Colors.red[900],),
                  SizedBox(width: 15,),
                  Text("Dashboard",
                    style: GoogleFonts.concertOne(
                    fontWeight: FontWeight.w500,
                        color: Colors.red[900],
                        fontSize: 20
                  ),),
                ],
              ),
            ),
            Divider(thickness: 2,),
            SizedBox(height: 10,),

            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.workspaces_filled, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Actions",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],

              ),
              dense: true,
              onTap: () {
                // Navigator.of(context).pop();

                var route= new MaterialPageRoute(
                    builder: (BuildContext context)=>
                    new Activity()
                );
                Navigator.of(context).push(route);
              },

            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.analytics, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Assets",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],

              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=1;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.keyboard_arrow_down_sharp),
            ),
            // Divider(height: 0),
            new ListTile(
            title: Row(
                children: [
                // SizedBox(width: 1, ),

                  Icon(Icons.save, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Licenses",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=2;
                });
                Navigator.of(context).pop();
              },
              // leading: Icon(Icons.save),
            ),
            // Divider(height: 0),
            new ListTile(
              title:   Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.keyboard, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Accessories",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              // new Text("" ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=3;
                });
                Navigator.of(context).pop();
              },
              // leading: ,
            ),
            // Divider(height: 0),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Transform.rotate(
                      angle: 3.14,
                      child: Icon(Icons.location_on, color: Colors.blueGrey[700],)),
                  SizedBox(width: 15,),
                  Text("Consumables",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.weekend, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Component",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.aspect_ratio, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Predefined Kits",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.people, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("People",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.cloud_download, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Import",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.settings, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Settings",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.keyboard_arrow_down_sharp),

            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.bar_chart_outlined, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Report",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.keyboard_arrow_down_sharp),

            ),
            new ListTile(
              title: Row(
                children: [
                  // SizedBox(width: 1, ),

                  Icon(Icons.computer_outlined, color: Colors.blueGrey[700],),
                  SizedBox(width: 15,),
                  Text("Requestable",
                    style: GoogleFonts.concertOne(
                        fontWeight: FontWeight.w500,
                        fontSize: 15
                    ),),
                ],
              ),
              dense: true,
              onTap: () {
                setState(() {
                  _currentIndex=0;
                });
                Navigator.of(context).pop();
              },
            ),

          ],

        ),
      ),

    );
  }
}
