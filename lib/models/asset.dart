import 'package:flutter/cupertino.dart';

class AssetsModel {
  String admin;
  String action;
  String item;
  String date;
  String target;

  AssetsModel({this.admin, this.action,this.item,  this.date,this.target});


}


List<AssetsModel> assets = assetsInfo
    .map((item) => AssetsModel(

    admin: item['admin'],
    action: item['action'],
    item: item['item'],
    date: item['date'],
    target: item['target'],
)
).toList();


var assetsInfo =[
  {
    "date": "Thu Dec 31, 2020 5:36AM",
    "admin": "Admin User",
    "action": "checkout",
    "item": "Cardstock (White)",
    "target": 'Jacky Murazik',

  },{
    "date": "Thu Dec 31, 2020 5:24AM",
    "admin": "",
    "action": "requested",
    "item": "Tab3 (1017522402)",
    "target": 'Admin User',

  },{
    "date": "Thu Dec 31, 2020 5:24AM",
    "admin": "Admin User",
    "action": "requested",
    "item": "Ultrafine 4k (525988554)",
    "target": 'Admin User',

  },{
    "date": "Thu Dec 31, 2020 5:23AM",
    "admin": "Admin User",
    "action": "checkout",
    "item": "Photoshop",
    "target": 'Chaz Frami',

  },{
    "date": "Thu Dec 31, 2020 5:10AM",
    "admin": "Admin User",
    "action": "checkout",
    "item": "USB Keyboard",
    "target": 'Ivory Bartell',

  },{
    "date": "Thu Dec 31, 2020 3:53AM",
    "admin": "Admin User",
    "action": "checkin from",
    "item": "Aaaa (949525123)",
    "target": 'Eunice Hills',

  },

];

