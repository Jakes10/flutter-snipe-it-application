import 'package:flutter/cupertino.dart';

class CategoryModel {
  String name;
  String type;
  String quantity;

  CategoryModel({this.name, this.type,this.quantity});


}


List<CategoryModel> category = categoryInfo
    .map((item) => CategoryModel(

  name: item['name'],
  type: item['type'],
  quantity: item['quantity'],
)
).toList();


var categoryInfo =[
  {
    "name": "Conference Phones",
    "type": "Asset",
    "quantity": '0',

  }, {
    "name": "Graphics Software",
    "type": "License",
    "quantity": '3',

  }, {
    "name": "HDD/SSD",
    "type": "Component",
    "quantity": '3',

  }, {
    "name": "Keyboards",
    "type": "Accessory",
    "quantity": '2',

  },{
    "name": "Office Software",
    "type": "License",
    "quantity": '1',

  },{
    "name": "Printer Ink",
    "type": "Consumable",
    "quantity": '1',

  },{
    "name": "RAM",
    "type": "Component",
    "quantity": '2',

  },

];

